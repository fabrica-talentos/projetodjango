from django.contrib import admin

# Register your models here.

from comic_books.models import ComicBook


class ComicBookAdmin(admin.ModelAdmin):
    pass


admin.site.register(ComicBook, ComicBookAdmin)