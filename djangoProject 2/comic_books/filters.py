import django_filters
from django import forms

from comic_books.models import ComicBook


class ComicBooksFilter(django_filters.FilterSet):
    title = django_filters.CharFilter(
        lookup_expr='icontains',
        widget=forms.TextInput(
            attrs={
                'class': 'bg-app-gray-400 rounded-none px-3 placeholder:font-semibold placeholder:font-roboto '
                         'placeholder:text-app-blue-300 py-1 placeholder:text-sm',
                'placeholder': 'INPUT THE TITLE'
            }
        )
    )
    publicationYear = django_filters.NumberFilter(
        lookup_expr='icontains',
        widget=forms.TextInput(
            attrs={
                'class': 'bg-app-gray-400 rounded-none px-3 placeholder:font-semibold placeholder:font-roboto '
                         'placeholder:text-app-blue-300 py-1 placeholder:text-sm',
                'placeholder': 'INPUT THE YEAR OF THE PUBLICATION'
            }
        )
    )
    price = django_filters.NumberFilter(
        lookup_expr='icontains',
        widget=forms.NumberInput(
            attrs={
                'class': 'bg-app-gray-400 rounded-none px-3 placeholder:font-semibold placeholder:font-roboto '
                         'placeholder:text-app-blue-300 py-1 placeholder:text-sm',
                'placeholder': 'INPUT THE PRICE'
            }
        )
    )

    situation = django_filters.CharFilter(
        lookup_expr='exact',
        widget=forms.TextInput(
            attrs={
                'class': 'bg-app-gray-400 rounded-none px-3 placeholder:font-semibold placeholder:font-roboto '
                         'placeholder:text-app-blue-300 py-1 placeholder:text-sm hidden',
                'placeholder': 'INPUT THE TITLE'
            }
        )
    )

    class Meta:
        model = ComicBook
        fields = ['title', 'publicationYear', 'price']
