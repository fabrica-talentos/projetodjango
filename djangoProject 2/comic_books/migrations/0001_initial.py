# Generated by Django 4.2.11 on 2024-04-18 13:33

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ComicBook',
            fields=[
                ('comic_books_id', models.UUIDField(auto_created=True, primary_key=True, serialize=False)),
                ('title', models.CharField(max_length=64)),
                ('author', models.CharField(max_length=64)),
                ('publication_year', models.IntegerField()),
                ('pages', models.IntegerField()),
                ('description', models.TextField(max_length=1024)),
                ('price', models.DecimalField(decimal_places=2, max_digits=10)),
                ('situation', models.CharField(default='new', max_length=20)),
                ('photo_comic_book', models.ImageField(upload_to='photos/')),
            ],
        ),
    ]
