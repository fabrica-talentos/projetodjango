from django.db import models


class ComicBook(models.Model):

    comic_books_id = models.UUIDField(primary_key=True, null=False, blank=False, auto_created=True)
    title = models.CharField(max_length=64)
    author = models.CharField(max_length=64)
    publication_year = models.IntegerField()
    pages = models.IntegerField()
    description = models.TextField(max_length=1024)
    price = models.DecimalField(decimal_places=2, max_digits=10)

    situation = models.CharField(max_length=20, default="new")

    photo_comic_book = models.ImageField(upload_to='photos/')

    def to_dict(self):
        return {
            "car_id": str(self.comic_books_id),
            "title": self.title,
            "publication_year": self.publication_year,
            "pages": self.pages,
            "description": self.description,
            "price": self.price,
            "situation": self.situation,
            "photo_comic_book": self.photo_comic_book,
        }
