from django.urls import path, re_path
from comic_books.views import ComicBooksDetailView, ComicBooksListView, ComicBooksCreateView, ComicBooksDeleteView

urlpatterns = [
    path(r'', ComicBooksListView.as_view(), name='comic-books-list'),
    path('register/', ComicBooksCreateView.as_view(), name='comic-books-create'),
    path('<str:car_id>/', ComicBooksDeleteView.as_view(), name='comic-books-delete'),
    path('<str:car_id>/detail', ComicBooksDetailView.as_view(), name='comic-books-detail')
]
