from uuid import uuid4

from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from django.views.generic import View

from comic_books.filters import ComicBooksFilter
from comic_books.forms import ComicBooksForms
from comic_books.models import ComicBook


class ComicBooksListView(View):
    def get(self, request):
        filter_form = ComicBooksFilter(request.GET, request=request)

        return render(request, "comic_books/list.html", {
            "extraHeadContext": {
                "title": "Comic Books List"
            },
            "context": {
                "situation": request.GET.get("situation"),
                "titleOfPage": request.GET.get("situation", "").upper(),
                "filter": filter_form
            }
        })


class ComicBooksCreateView(View):
    def post(self, request):
        create_form = ComicBooksForms(request.POST, request.FILES)
        if create_form.is_valid():
            create_form.save()

            return redirect(
                f"{reverse('comic-books-list')}?situation={request.POST.get('situation')}"
            )

        return render(request, "comic_books/form.html", {
            "extraHeadContext": {
                "title": "Comic Books Create"
            },
            "context": {
                "situation": request.GET.get("situation"),
                "titleOfPage": request.GET.get("situation", "").upper(),
                "form": create_form
            }
        })

    def get(self, request):
        create_form = ComicBooksForms(
            initial={
                "comic_book_id": str(uuid4())
            }
        )

        return render(request, "comic_books/form.html", {
            "extraHeadContext": {
                "title": "Comic Books Create"
            },
            "context": {
                "situation": request.GET.get("situation"),
                "titleOfPage": request.GET.get("situation", "").upper(),
                "form": create_form
            }
        })


class ComicBooksDeleteView(View):
    def get(self, request, comic_book_id):
        comic_book = ComicBook.objects.filter(comic_books_id=comic_book_id)
        comic_book.delete()

        return redirect(
            f"{reverse('comic-books-list')}?situation={request.GET.get('situation')}"
        )


class ComicBooksDetailView(View):
    def get(self, request, car_id):
        comic_book = get_object_or_404(ComicBook, car_id=car_id)
        detail_form = ComicBooksForms(detail=True, initial=comic_book.to_dict())

        return render(request, "comic_books/form.html", {
            "extraHeadContext": {
                "title": "Comic Books Detail"
            },
            "context": {
                "situation": request.GET.get("situation"),
                "titleOfPage": comic_book.situation.upper(),
                "mode": 'detail',
                "form": detail_form
            }
        })
