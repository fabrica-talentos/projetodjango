from uuid import uuid4

from django import forms

from comic_books.models import ComicBook


class ComicBooksForms(forms.ModelForm):
    comic_books_id = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class': 'bg-app-gray-400 rounded-none px-3 placeholder:font-semibold placeholder:font-roboto '
                         'placeholder:text-app-blue-300 py-1 placeholder:text-sm hidden',
                'placeholder': 'INPUT THE TITLE',
                'readonly': True
            }
        )
    )

    title = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class': 'bg-app-gray-400 rounded-none px-3 placeholder:font-semibold placeholder:font-roboto '
                         'placeholder:text-app-blue-300 py-1 placeholder:text-sm',
                'placeholder': 'INPUT THE TITLE'
            }
        )
    )

    publication_year = forms.CharField(
        widget=forms.NumberInput(
            attrs={
                'class': 'bg-app-gray-400 rounded-none px-3 placeholder:font-semibold placeholder:font-roboto '
                         'placeholder:text-app-blue-300 py-1 placeholder:text-sm',
                'placeholder': 'INPUT THE YEAR OF THE PUBLICATION',
            }
        )
    )

    pages = forms.CharField(
        widget=forms.NumberInput(
            attrs={
                'class': 'bg-app-gray-400 rounded-none px-3 placeholder:font-semibold placeholder:font-roboto '
                         'placeholder:text-app-blue-300 py-1 placeholder:text-sm',
                'placeholder': 'INPUT THE NUMBER OF PAGES',
            }
        )
    )

    description = forms.CharField(
        widget=forms.Textarea(
            attrs={
                'class': 'bg-app-gray-400 rounded-none px-3 placeholder:font-semibold placeholder:font-roboto '
                         'placeholder:text-app-blue-300 py-2 placeholder:text-sm w-full h-36',
                'placeholder': 'DESCRIBE THE MAIN INFORMATION ABOUT THE COMIC BOOK'
            }
        )
    )

    price = forms.DecimalField(
        widget=forms.NumberInput(
            attrs={
                'class': 'bg-app-gray-400 rounded-none px-3 placeholder:font-semibold placeholder:font-roboto '
                         'placeholder:text-app-blue-300 py-1 placeholder:text-sm',
                'placeholder': 'INPUT THE PRICE'
            }
        )
    )

    situation = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class': 'bg-app-gray-400 rounded-none px-3 placeholder:font-semibold placeholder:font-roboto '
                         'placeholder:text-app-blue-300 py-2 placeholder:text-sm w-full hidden',
                'placeholder': 'DESCRIBE THE MAIN INFORMATION ABOUT THE COMIC BOOK'
            }
        )
    )

    photo_comic_book = forms.ImageField()

    class Meta:
        model = ComicBook
        fields = '__all__'

    def __init__(self, detail: bool = False, *args, **kwargs):
        super(ComicBooksForms, self).__init__(*args, **kwargs)

        if detail:
            for _, field in self.fields.items():
                field.widget.attrs["disabled"] = True
